const gulp = require('gulp');
const sass = require('gulp-sass');
const image = require('gulp-image');
const concat = require('gulp-concat');
const pug = require('gulp-pug');
const browserSync = require('browser-sync');
const autoprefixer = require('gulp-autoprefixer');
const uglify = require('gulp-uglify');
const pipeline = require('readable-stream').pipeline;
const babel = require('gulp-babel');

gulp.task('image', function () {
    gulp.src('./src/assets/img/*')
        .pipe(image())
        .pipe(gulp.dest('./build/assets/img'));
});

gulp.task('html', function () {
    gulp.src('./src/**/*.html')
        .pipe(gulp.dest('./build'));
});

gulp.task('pug', function () {
    gulp.src('./src/pages/**/*.pug')
        .pipe(pug())
        .pipe(concat('index.html'))
        .pipe(gulp.dest('./build'));
});

gulp.task('sass', function () {
    return gulp.src('./src/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('style.css'))
        .pipe(autoprefixer(['last 15 versions', '>1%']))
        .pipe(gulp.dest('./build/styles'));
});

gulp.task('uglify', function() {
    gulp.src('./src/js/**/*.js')
        .pipe(babel({
            presets: ['@babel/env']
        }))
        // .pipe(uglify().on('error', function(e){
        //     console.log(e);
        // }))
        .pipe(gulp.dest('./build/js'));
  });


gulp.task('browserSync', function () {
    browserSync({
        server: {
            baseDir: './build/'
        }
    })
});

gulp.task('watch', ['sass', 'html', 'pug', 'image', 'uglify', 'browserSync'], function () {
    gulp.watch('./src/styles**/*.scss', ['sass']);
    gulp.watch('./src/assets/img/*', ['image']);
    gulp.watch('./src/**/*.html', ['html']);
    gulp.watch('./src/pages/**/*.pug', ['pug']);
    gulp.watch('./src/js/**/*.js', ['uglify']);
    gulp.watch('build/*.html', browserSync.reload);
    gulp.watch("./build/**/*.css").on("change", browserSync.reload);
    gulp.watch('./build/**/*.js').on("change", browserSync.reload);
});

gulp.task('default', ['watch', 'image']);