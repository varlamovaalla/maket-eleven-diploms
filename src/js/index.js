// featured projects

let projects = [
    { name: 'HTML',
    way: 'URL(\"assets/img/html.jpg\")' },
    { name: 'JOOMLA',
    way: 'URL(\"assets/img/joomla.jpg\")' },
    { name: 'WORDPRESS',
    way: 'URL(\"assets/img/wordpress.jpg\")' },
    { name:  'OPENCART',
    way: 'URL(\"assets/img/opencart.jpg\")' },
    { name:  'LOGO',
    way: 'URL(\"assets/img/logo.jpg\")' },
    { name:  'PRINT',
    way: 'URL(\"assets/img/print.jpg\")' },
    { name:  'HTML',
    way: 'URL(\"assets/img/html2.jpg\")' },
    { name:  'HTML',
    way: 'URL(\"assets/img/html3.jpg\")' }
];

let projectsDiv = document.getElementsByClassName('picturefeaturedProject');

console.log(projectsDiv);

let projectsButtonOne = document.getElementById('ALL WORKS');
let projectsButtonTwo = document.getElementById('HTML');
let projectsButtonThree = document.getElementById('JOOMLA');
let projectsButtonFour = document.getElementById('WORDPRESS');
let projectsButtonFive = document.getElementById('OPENCART');
let projectsButtonSix = document.getElementById('LOGO');
let projectsButtonSeven = document.getElementById('PRINT');

let k;
let i;

function NoclickProjectButton () {
  k=0;
  while(k<projectsDiv.length) {
      i = Math.floor(Math.random()*projects.length);
      projectsDiv[k].style.backgroundImage = projects[i].way;
      k++;
}}
NoclickProjectButton();

function clickProjectButton (NameButton) {
    k=0;
    while(k<projectsDiv.length) {
        i = Math.floor(Math.random()*projects.length);
        if (projects[i].name == NameButton) {
            projectsDiv[k].style.backgroundImage = projects[i].way;
            k++;}
}}
function clickProjectButtonAll() {
    k=0;
    while(k<projectsDiv.length) {
    i = Math.floor(Math.random()*projects.length);
    projectsDiv[k].style.backgroundImage = projects[i].way;
    k++;
}}

projectsButtonOne.addEventListener('click', clickProjectButtonAll);
projectsButtonTwo.addEventListener('click', function(){clickProjectButton('HTML')});
projectsButtonThree.addEventListener('click', function(){clickProjectButton('JOOMLA')});
projectsButtonFour.addEventListener('click', function(){clickProjectButton('WORDPRESS')});
projectsButtonFive.addEventListener('click', function(){clickProjectButton('OPENCART')});
projectsButtonSix.addEventListener('click', function(){clickProjectButton('LOGO')});
projectsButtonSeven.addEventListener('click', function(){clickProjectButton('PRINT')});

//projects load more

let moreDiv = document.getElementsByClassName('featuredProject_loadMore');
let projectsAll = document.getElementsByClassName('featuredProject');

let projectsloadButton = document.getElementsByClassName('loadmore');

console.log(projectsloadButton);

function plusRows() {
    moreDiv[0].style.display = 'flex';
    projectsAll[0].classList.add('featuredProjectClickOne');
    projectsAll[0].classList.remove('featuredProjectClickTwo');
    projectsloadButton[0].style.display = 'none';
    projectsloadButton[1].style.display = 'block';
    projectsloadButton[2].style.display = 'block';
    projectsloadButton[3].style.display = 'none';
}

function plusRowsTwo() {
  moreDiv[1].style.display = 'flex';
  projectsAll[0].classList.add('featuredProjectClickTwo');
  projectsAll[0].classList.remove('featuredProjectClickOne');
  projectsloadButton[0].style.display = 'none';
  projectsloadButton[1].style.display = 'none';
  projectsloadButton[2].style.display = 'none';
  projectsloadButton[3].style.display = 'block';
}

function deleteMoreProjectsTwo() {
  moreDiv[1].style.display = 'none';
  projectsAll[0].classList.remove('featuredProjectClickTwo');
  projectsAll[0].classList.add('featuredProjectClickOne');
  projectsloadButton[0].style.display = 'none';
  projectsloadButton[1].style.display = 'block';
  projectsloadButton[2].style.display = 'block';
  projectsloadButton[3].style.display = 'none';
}

function deleteMoreProjects() {
  moreDiv[0].style.display = 'none';
  projectsAll[0].classList.remove('featuredProjectClickOne');
  projectsloadButton[0].style.display = 'block';
  projectsloadButton[1].style.display = 'none';
  projectsloadButton[2].style.display = 'none';
  projectsloadButton[3].style.display = 'none';
}

// PROJECTS mobile all

let allButtonPartTwoMedium = document.getElementsByClassName('button_part_twoJSMedium');
console.log(allButtonPartTwoMedium);

let slideIndexButtonProjectMedium = 1;
showSlidesButtonProjectMedium(slideIndexButtonProjectMedium);

function plusButtonProjectMedium(b) {
  showSlidesButtonProjectMedium(slideIndexButtonProjectMedium += b);
}

function showSlidesButtonProjectMedium(b) {
  let i;
  if (b > allButtonPartTwoMedium.length) {
    slideIndexButtonProjectMedium = 1;
  } else if (b < 1) {
    slideIndexButtonProjectMedium = allButtonPartTwoMedium.length;
  }
  for (i = 0; i < allButtonPartTwoMedium.length; i++) {
    allButtonPartTwoMedium[i].style.display = 'none'; 
  }
  if (slideIndexButtonProjectMedium < allButtonPartTwoMedium.length){
    allButtonPartTwoMedium[slideIndexButtonProjectMedium-1].style.display = 'block';
    allButtonPartTwoMedium[slideIndexButtonProjectMedium].style.display = 'block';
  } else {
    allButtonPartTwoMedium[slideIndexButtonProjectMedium-1].style.display = 'block';
    allButtonPartTwoMedium[0].style.display = 'block';
  }
}

let projectsDivMobileMedium = document.getElementsByClassName('picturefeaturedProjectMobileMedium');
console.log(projectsDivMobileMedium);
console.log(projectsDivMobileMedium);

let slideIndexProjectPictureMedium = 1;
showSlidesProjectPictureMedium(slideIndexProjectPictureMedium);

function plusProjectPictureMedium(p) {
  showSlidesProjectPictureMedium(slideIndexProjectPictureMedium += p);
}

function showSlidesProjectPictureMedium(p) {
  let i;
  if (p > projectsDivMobileMedium.length) {
    slideIndexProjectPictureMedium = 1;
  } else if (p < 1) {
    slideIndexProjectPictureMedium = projectsDivMobileMedium.length;
  }
  for (i = 0; i < projectsDivMobileMedium.length; i++) {
    projectsDivMobileMedium[i].style.display = 'none';
  }
  if (slideIndexProjectPictureMedium < projectsDivMobileMedium.length){
    projectsDivMobileMedium[slideIndexProjectPictureMedium-1].style.display = 'block';
    projectsDivMobileMedium[slideIndexProjectPictureMedium].style.display = 'block';
  } else {
    projectsDivMobileMedium[slideIndexProjectPictureMedium-1].style.display = 'block';
    projectsDivMobileMedium[0].style.display = 'block';
  }
}

// PROJECTS mobile 320-479px

let allButtonPartTwo = document.getElementsByClassName('button_part_twoJS');

let slideIndexButtonProject = 1;
showSlidesButtonProject(slideIndexButtonProject);

function plusButtonProject(b) {
  showSlidesButtonProject(slideIndexButtonProject += b);
}

function showSlidesButtonProject(b) {
  let i;
  if (b > allButtonPartTwo.length) {
    slideIndexButtonProject = 1;
  } else if (b < 1) {
    slideIndexButtonProject = allButtonPartTwo.length;
  }
  for (i = 0; i < allButtonPartTwo.length; i++) {
    allButtonPartTwo[i].style.display = 'none'; 
  }
  allButtonPartTwo[slideIndexButtonProject-1].style.display = 'block';
}

let projectsDivMobile = document.getElementsByClassName('picturefeaturedProjectMobile');

let slideIndexProjectPicture = 1;
showSlidesProjectPicture(slideIndexProjectPicture);

function plusProjectPicture(p) {
  showSlidesProjectPicture(slideIndexProjectPicture += p);
}

function showSlidesProjectPicture(p) {
  let i;
  if (p > projectsDivMobile.length) {
    slideIndexProjectPicture = 1;
  } else if (p < 1) {
    slideIndexProjectPicture = projectsDivMobile.length;
  }
  for (i = 0; i < projectsDivMobile.length; i++) {
    projectsDivMobile[i].style.display = 'none';
  }
  projectsDivMobile[slideIndexProjectPicture-1].style.display = 'block';
}


// price

let priceDiv = document.getElementsByClassName('fullColumn');

console.log(priceDiv);

function hoverPriceDiv (numberPrice) {
    priceDiv[numberPrice].style.height = '815px';
    priceDiv[numberPrice].style.gridTemplateRows = '375px 440px';
}

function notHoverPriceDiv (numberPrice) {
    priceDiv[numberPrice].style.height = '715px';
    priceDiv[numberPrice].style.gridTemplateRows = '325px 390px';
}

priceDiv[0].addEventListener('mousemove', function(){hoverPriceDiv(0)});
priceDiv[1].addEventListener('mousemove',function(){hoverPriceDiv(1)});
priceDiv[2].addEventListener('mousemove', function(){hoverPriceDiv(2)});

priceDiv[0].addEventListener('mouseout', function(){notHoverPriceDiv(0)});
priceDiv[1].addEventListener('mouseout',function(){notHoverPriceDiv(1)});
priceDiv[2].addEventListener('mouseout', function(){notHoverPriceDiv(2)});

// header yak

let yak = document.querySelectorAll('a[href*="#"]');
console.log(yak);
let divYak = document.getElementById('yak');
let animationTime = 5000;
let framesCount = 500;
for (let a = 0; a < yak.length; a++) {
    yak[a].addEventListener('click', function (e) {
    e.preventDefault();
    let coordY = divYak.getBoundingClientRect().top + 550;
    let scroller = setInterval(function() {
    let scrollBy = coordY / framesCount;
    if(scrollBy > (window.pageYOffset - coordY) && (window.innerHeight + window.pageYOffset) < document.body.offsetHeight) {
      window.scrollBy(0, scrollBy);
    } else {
      window.scrollTo(0, coordY);
      clearInterval(scroller);
    }
  }, animationTime / framesCount);
});
};

// header menu

let headerMenu = document.getElementsByClassName('headerMenu');
let menuPicture = document.getElementsByClassName('header_menuCenter');
console.log(menuPicture);
console.log(headerMenu);
let h = headerMenu.length/2;
let m;

function beforeDisplayMenu(m) {
  setTimeout(function() {
    headerMenu[m].classList.add('headerMenuBlock');
  }, 800*(m+1));
}

function beforeDisplayMenuTwo(h) {
  setTimeout(function() {
    headerMenu[h].classList.add('headerMenuBlock');
  }, 800*(h-5+1));
}

function displayMenu() {
  for(m = 0; m < h; m++) {
    beforeDisplayMenu(m);
  }
  for(h; h < headerMenu.length; h++) {
    beforeDisplayMenuTwo(h);
  }
}

menuPicture[0].addEventListener('mousemove', displayMenu);

// about

let divCol = document.getElementsByClassName('about_part_colJS');
let divAbout = document.getElementsByClassName('about');
let b;
let readyAbout = false;
let readyFact = false;
let coordYdivAbout = divAbout[0].getBoundingClientRect().top;
let clearAbout = divAbout[0].getBoundingClientRect().bottom + window.pageYOffset;

console.log(coordYdivAbout);
console.log(clearAbout);
console.log(window.pageYOffset);

function displayAbout(b) {
  setTimeout(function() {
    divCol[b].classList.add('about_part_col__Animaton');
  }, 1000*(b+1));
}

function displayAboutAnim() {
  for(b=0; b < divCol.length; b++) {
    displayAbout(b);
    if (b === divCol.length - 1){
      readyAbout = false;
    }
  }
}

// facts 

let divColFact = document.getElementsByClassName('fact__col_JS');
let divFact = document.getElementsByClassName('fact');
let f;
let coordYdivFact = divFact[0].getBoundingClientRect().top + window.pageYOffset - 400;
let cleardivFact = divFact[0].getBoundingClientRect().bottom + window.pageYOffset;

// console.log(coordYdivFact);
// console.log(cleardivFact);
// console.log(window.pageYOffset);

function displayFact(f) {
  setTimeout(function() {
    divColFact[f].classList.add('fact__Anim');
  }, 1000*(f+1));
}

function displayFactAnim() {
  for(f=0; f < divColFact.length; f++) {
    displayFact(f);
    if (f === divColFact.length - 1){
      readyFact = false;
    }
  }
}

window.onscroll = function inVisible() {
  if(!readyAbout){
    readyAbout = true;
    for(let z= (divCol.length-1); z >=0; z--)  {
    if (divCol[z].classList.contains('about_part_col__Animaton') && (window.pageYOffset > clearAbout) || (window.pageYOffset < coordYdivAbout)) {
      divCol[z].classList.remove('about_part_col__Animaton');
      if (z === divCol.length - 1 ) {
        readyAbout = false;
      }
    } else {
      displayAboutAnim();
    }
  }}
  if(!readyFact){
    readyFact = true;
  for(let x= (divColFact.length-1); x >=0; x--)  {
    if (divColFact[x].classList.contains('fact__Anim') && ((window.pageYOffset > cleardivFact) || (window.pageYOffset < coordYdivFact))) {
      divColFact[x].classList.remove('fact__Anim');
      if (x === divColFact.length - 1 ) {
        readyFact = false;
      }
    } else {
      displayFactAnim();
    }
  }}
}

//work 

let divRow = document.getElementsByClassName('workRow');
let divPicture = document.getElementsByClassName('workDirection');
let NumberWork;
let ready = false;
console.log(divRow);
console.log(divPicture);

function workDirectionEven (NumberWork) {
  if (NumberWork%2 == 0){
  setTimeout(function() {
    divRow[NumberWork].classList.add('work__Animaton');
    if (NumberWork === divRow.length - 1) {
      ready = false;
    }
  }, 700*(NumberWork+1));} else {
    setTimeout(function() {
      divRow[NumberWork].classList.remove('work__Animaton');
      if (NumberWork === divRow.length - 1) {
        ready = false;
      }
    }, 700*(NumberWork+1));
  }
}
function workDirectionNoEven (NumberWork) {
  if (NumberWork%2 == 0){
  setTimeout(function() {
    divRow[NumberWork].classList.remove('work__Animaton');
    if (NumberWork === divRow.length - 1) {
      ready = false;
    }
  }, 700*(NumberWork+1));} else {
    setTimeout(function() {
      divRow[NumberWork].classList.add('work__Animaton');
      if (NumberWork === divRow.length - 1) {
        ready = false;
      }
    }, 700*(NumberWork+1));
  }
}
function callWorkDirection() {
  if(!ready) {
    ready=true;
    for (NumberWork = 0; NumberWork<divRow.length; NumberWork++) {
      workDirectionEven (NumberWork);
    }
  }
}
function callWorkReverseDirection() {
setTimeout(function() {
  for (NumberWork = 0; NumberWork<divRow.length; NumberWork++) {
    workDirectionNoEven (NumberWork);
  }
}, 700*divPicture.length);}

for(let NumberRow = 0; NumberRow < divPicture.length; NumberRow++){
 divPicture[NumberRow].addEventListener('mouseenter', callWorkDirection);
 divPicture[NumberRow].addEventListener('mouseout', callWorkReverseDirection);
}
// header menu mobile

let slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function showSlides(n) {
  let i;
  let slides = document.getElementsByClassName('headerMenuMobile');
  if (n > slides.length) {
    slideIndex = 1
  } else if (n < 1) {
    slideIndex = slides.length
  }
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = 'none'; 
  }
  slides[slideIndex-1].style.display = 'block'; 
}

// about mobile 320-479px

let slideIndexAbout = 1;
showSlidesAbout(slideIndexAbout);

function plusSlidesAbout(s) {
  showSlidesAbout(slideIndexAbout += s);
}

function showSlidesAbout(s) {
  let i;
  let slidesAbout = document.getElementsByClassName('about_part_colMobile');
  if (s > slidesAbout.length) {
    slideIndexAbout = 1
  } else if (s < 1) {
    slideIndexAbout = slidesAbout.length
  }
  for (i = 0; i < slidesAbout.length; i++) {
      slidesAbout[i].style.display = 'none'; 
  }
  slidesAbout[slideIndexAbout-1].style.display = 'grid'; 
}

// about mobile all

let slideIndexAboutMedium = 1;
showSlidesAboutMedium(slideIndexAboutMedium);

function plusSlidesAboutMedium(s) {
  showSlidesAboutMedium(slideIndexAboutMedium += s);
}

function showSlidesAboutMedium(s) {
  let i;
  let slidesAboutMedium = document.getElementsByClassName('about_part_colMobileMedium');
  if (s > slidesAboutMedium.length) {
    slideIndexAboutMedium = 1
  } else if (s < 1) {
    slideIndexAboutMedium = slidesAboutMedium.length
  }
  for (i = 0; i < slidesAboutMedium.length; i++) {
      slidesAboutMedium[i].style.display = 'none'; 
  }  
  if (slideIndexAboutMedium < slidesAboutMedium.length){
    slidesAboutMedium[slideIndexAboutMedium-1].style.display = 'grid';
    slidesAboutMedium[slideIndexAboutMedium].style.display = 'grid';
  } else {
    slidesAboutMedium[slideIndexAboutMedium-1].style.display = 'grid';
    slidesAboutMedium[0].style.display = 'grid';
  }
}

//fact mobile all

let slideIndexFact = 1;
showSlidesFact(slideIndexFact);

function plusSlidesFact(t) {
  showSlidesFact(slideIndexFact += t);
}

function showSlidesFact(t) {
  let i;
  let slidesFact = document.getElementsByClassName('fact__col_mobile');
  if (t > slidesFact.length) {
    slideIndexFact = 1
  } else if (t < 1) {
    slideIndexFact = slidesFact.length
  }
  for (i = 0; i < slidesFact.length; i++) {
      slidesFact[i].style.display = 'none'; 
  }
  slidesFact[slideIndexFact-1].style.display = 'grid'; 
}

//fact mobile 480-767px

let slideIndexFactMedium = 1;
showSlidesFactMedium(slideIndexFactMedium);

function plusSlidesFactMedium(t) {
  showSlidesFactMedium(slideIndexFactMedium += t);
}

function showSlidesFactMedium(t) {
  let i;
  let slidesFactMedium = document.getElementsByClassName('fact__col_mobileMedium');
  if (t > slidesFactMedium.length) {
    slideIndexFactMedium = 1
  } else if (t < 1) {
    slideIndexFactMedium = slidesFactMedium.length
  }
  for (i = 0; i < slidesFactMedium.length; i++) {
      slidesFactMedium[i].style.display = 'none'; 
  }
  if (slideIndexFactMedium < slidesFactMedium.length) {
    slidesFactMedium[slideIndexFactMedium-1].style.display = 'grid';
    slidesFactMedium[slideIndexFactMedium].style.display = 'grid';
  } else {
    slidesFactMedium[slideIndexFactMedium-1].style.display = 'grid';
    slidesFactMedium[0].style.display = 'grid';
  }
}

//WORK mobile

let slideIndexWork = 1;
showSlidesWork(slideIndexWork);

function plusSlidesWork(w) {
  showSlidesWork(slideIndexWork += w);
}

function showSlidesWork(w) {
  let i;
  let slidesWork = document.getElementsByClassName('workRowMobile');
  if (w > slidesWork.length) {
    slideIndexWork = 1
  } else if (w < 1) {
    slideIndexWork = slidesWork.length
  }
  for (i = 0; i < slidesWork.length; i++) {
      slidesWork[i].style.display = 'none'; 
  }
  slidesWork[slideIndexWork-1].style.display = 'grid'; 
}

//WORK mobile

let slideIndexPrice = 1;
showSlidesPrice(slideIndexPrice);

function plusSlidesPrice(p) {
  showSlidesPrice(slideIndexPrice += p);
}

function showSlidesPrice(p) {
  let i;
  let slidesPrice = document.getElementsByClassName('fullColumnMedium');
  if (p > slidesPrice.length) {
    slideIndexPrice = 1
  } else if (p < 1) {
    slideIndexPrice = slidesPrice.length
  }
  for (i = 0; i < slidesPrice.length; i++) {
      slidesPrice[i].style.display = 'none'; 
  }
  slidesPrice[slideIndexPrice-1].style.display = 'grid'; 
}

// BLOG mobile

let slideIndexBlog = 1;
showSlidesBlog(slideIndexBlog);

function plusSlidesBlog(l) {
  showSlidesBlog(slideIndexBlog += l);
}

function showSlidesBlog(l) {
  let i;
  let slidesBlog = document.getElementsByClassName('blogRowMobile');
  if (l > slidesBlog.length) {
    slideIndexBlog = 1
  } else if (l < 1) {
    slideIndexBlog = slidesBlog.length
  }
  for (i = 0; i < slidesBlog.length; i++) {
      slidesBlog[i].style.display = 'none'; 
  }
  slidesBlog[slideIndexBlog-1].style.display = 'flex'; 
}

// PEOPLE mobile

let slideIndexPeople = 1;
showSlidesPeople(slideIndexPeople);

function plusSlidesPeople(p) {
  showSlidesPeople(slideIndexPeople += p);
}

function showSlidesPeople(p) {
  let i;
  let slidesPeople = document.getElementsByClassName('people__allMobile');
  if (p > slidesPeople.length) {
    slideIndexPeople = 1
  } else if (p < 1) {
    slideIndexPeople = slidesPeople.length
  }
  for (i = 0; i < slidesPeople.length; i++) {
      slidesPeople[i].style.display = 'none'; 
  }
  slidesPeople[slideIndexPeople-1].style.display = 'flex'; 
}

// ROCKET mobile

let slideIndexRocket = 1;
showSlidesRocket(slideIndexRocket);

function plusSlidesRocket(r) {
  showSlidesRocket(slideIndexRocket += r);
}

function showSlidesRocket(r) {
  let i;
  let slidesRocket = document.getElementsByClassName('rocketRowMobile');
  if (r > slidesRocket.length) {
    slideIndexRocket = 1
  } else if (r < 1) {
    slideIndexRocket = slidesRocket.length
  }
  for (i = 0; i < slidesRocket.length; i++) {
      slidesRocket[i].style.display = 'none'; 
  }
  slidesRocket[slideIndexRocket-1].style.display = 'flex'; 
}

// form

let form = document.forms['contact'];
// let element = form.elements;

// function showError(container, errorMessage) {
//   container.classList.add('error');
//   container.setAttribute('placeholder', errorMessage);  
// }

// function resetError(container, errorMessageNew, errorMessageOld) {
//   let attributeNew = container.getAttribute('placeholder');
//   if (attributeNew == errorMessageNew) {
//     container.setAttribute('placeholder', errorMessageOld);
//   }
// }

// function validate() {

//   resetError(element.FullName, 'Write your Full Name', 'FullName');
//   if (!element.FullName.value) {
//     showError(element.FullName, 'Write your Full Name');
//   }

//   resetError(element.Email, 'Write your E-mail', 'Write your E-mail');
//   if (!element.Email.value) {
//     showError(element.Email, 'Write your E-mail');
//   }

//   resetError(element.WebSite, 'Write your Website', 'Write your Website');
//   if (!element.WebSite.value) {
//     showError(element.WebSite, 'Write your Website');
//   }

//   resetError(element.Code, 'Write Code', 'Write Code');
//   if (!element.Code.value) {
//     showError(element.Code, 'Write Code');
//   }
// }
let button = document.getElementById('send');
console.log(button);
console.log(form);
console.log(element);
function sss() {
  document.forms['contact'].submit();
}

  //fact mobile all

let slideIndexMini = 1;

function plusSlidesMini(number, className, typeOfdisplay) {
  showSlidesMini(slideIndexMini += number, className, typeOfdisplay);
}

function showSlidesMini(newAll, className, typeOfdisplay) {
  let i;
  let slides = document.getElementsByClassName(className);
  console.log(slides);
  if (newAll > slides.length) {
    slideIndexMini = 1
  } else if (newAll < 1) {
    slideIndexMini = slides.length
  }
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = 'none'; 
  }
  slides[slideIndexMini-1].style.display = typeOfdisplay;
}
